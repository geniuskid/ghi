class PatientsController < ApplicationController
  before_action :authenticate_staff!, :set_patient, only: [:show]
  def index
    @patients = Patient.all
  end

  def show
    @visit = @patient.visits.new
    @visits = @patient.visits
  end

  def new
    @patient = Patient.new
  end

  def edit
  end

  def create
    @patient = Patient.new(patient_params)

    respond_to do |format|
      if @patient.save
        format.html { redirect_to @patient, notice: 'Patient was successfully added.' }
      else
        format.html { render :new }
      end
    end
  end

  def create_visit
    @patient = Patient.find(params[:visit][:patient_id])
    @visit = @patient.visits.new(visit_params)

    respond_to do |format|
      if @visit.save
        format.html { redirect_to @patient, notice: 'Patient was successfully added.' }
      else
        format.html { render :show }
      end
    end
  end


  private
    def visit_params
      params.require(:visit).permit(:hospital_id, :patient_id, :diagnose_1, :diagnose_2, :diagnose_3, :diagnose_4, :diagnose_5, :diagnose_6, :treatment_on_site, :diagnose_on_site, :hemoglobin, :rbc, :htc, :mvc, :mch, :mchc, :rdw_cv, :rdw_sd, :wbc, :neuper, :granum, :plt, :esr, :symptoms_1, :symptoms_2, :symptoms_3, :symptoms_4, :symptoms_5, :symptoms_6, :prescription_1, :prescription_2, :prescription_3, :prescription_4, :prescription_5, :prescription_6 )
    end

    def set_patient
      @patient = Patient.find(params[:id])
    end

    def patient_params
      params.require(:patient).permit(:patient_first_name, :patient_num, :patient_last_name, :phone_number, :address, :dob)
    end

end
