class ApplicationController < ActionController::Base
before_action :set_subdomain


private

def set_subdomain
  unless request.subdomains.blank?
     @hospital = Hospital.where(subdomain: request.subdomain).first
  end
  # binding.pry
end

end
