class StaffsController < ApplicationController
  before_action :authenticate_staff!
  # before_action :admin_user, only: :destroy
 def new
  @staff = Staff.new
 end

  def index
    @staffs = Staff.all
  end

  def show
   @user = User.find(params[:id])
   @microposts = @user.microposts.paginate(page: params[:page])
  end

  def edit
         @user = User.find(params[:id])
  end

  def update
         @user = User.find(params[:id])
         if @user.update_without_password(user_params)
             flash[:success] = "Profile updated"
             redirect_to @user
         else
             render 'edit'
         end
  end

  def destroy
         User.find(params[:id]).destroy
         flash[:success] = "User deleted"
         redirect_to users_url
  end

  def create_staff
     @staff = Staff.new(staff_params)
     respond_to do |format|
       if @staff.save
         format.html { redirect_to staffs_index_path, notice: 'staff was successfully created.' }
       else
         format.html { render :new }
       end
     end
  end




 private
 def staff_params
     params.require(:staff).permit(:first_name, :email, :last_name, :password, :password_confirmation, :phone_number, :user_type)
 end
   # Before filters
   # Confirms a logged-in user.

   # Confirms the correct user.
   def correct_staff
       @staff = Staff.find(params[:id])
       redirect_to(root_url) unless current_staff?(@staff)
   end
   def admin_staff
      flash[:success] = "access denied."
       redirect_to(root_url) unless current_staff.administrator?
   end

end
