class SessionsController < Clearance::SessionsController
  auto_session_timeout_actions

  def destroy
    sign_out
    redirect_to root_path
  end

  private

  def url_after_destroyed
    root_url
  end

end
