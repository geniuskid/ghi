module ApplicationHelper
  def  hospital_name(hospitalid)
    Hospital.find(hospitalid.to_i).name
  end
end
