class Visit < ApplicationRecord
  belongs_to :patient
  belongs_to :hospital
 
  jsonb_accessor :appointment,
    doctor_name: :string,
    department: :string,
    appointment_time: :datetime
  jsonb_accessor :diagnose,
      patient_history: :string,
      diagnose_on_site: :string,
      treatment_on_site: :string,
      diagnose_1: :string,
      diagnose_2: :string,
      diagnose_3: :string,
      diagnose_4: :string,
      diagnose_5: :string,
      diagnose_6: :string
  jsonb_accessor :lab_results,
      doctor: :integer,
      hemoglobin: :float,
      rbc: :float,
      htc: :float,
      mvc: :float,
      mch: :float,
      mchc: :float,
      rdw_cv: :float,
      rdw_sd: :float,
      wbc: :float,
      neuper: :float,
      granum: :float,
      plt: :float,
      esr: :float
  jsonb_accessor :symptoms,
      symptoms_1: :string,
      symptoms_2: :string,
      symptoms_3: :string,
      symptoms_4: :string,
      symptoms_5: :string,
      symptoms_6: :string

  jsonb_accessor :prescription,
      prescription_1: :string,
      prescription_2: :string,
      prescription_3: :string,
      prescription_4: :string,
      prescription_5: :string,
      prescription_6: :string






end
