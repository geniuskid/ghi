class Hospital < ApplicationRecord
  has_many :visits
  after_create :set_subdomain
  after_destroy_commit	:remove_subdomain
  RESTRICTED_SUBDOMAIN = %w(www)
 validates :subdomain, presence: true,
                            uniqueness: { case_sensitive: false},
                            format: { with: /\A[\w\-]+\Z/i, message: 'contains invalid characters' },
                            exclusion: {in: RESTRICTED_SUBDOMAIN, message: 'restricted'}

    before_validation :downcase_account

  private
        def downcase_account
            self.subdomain = subdomain.try(:downcase)
        end

  def set_subdomain
    Apartment::Tenant.create(self.subdomain)

    Apartment::Tenant.switch(self.subdomain) do
      Staff.create! :email => 'admin@example.com', :password => '123456', :password_confirmation => '123456'
    end

  end

  def remove_subdomain
    Apartment::Tenant.drop(self.subdomain)
  end

end
