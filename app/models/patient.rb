class Patient < ApplicationRecord
  before_create :set_patient_num

  has_many :visits, dependent: :destroy
  jsonb_accessor :personal_details,
    patient_num: :string,
    patient_first_name: :string,
    patient_last_name: :string,
    phone_number: :string,
    address: :string,
    dob: :date

    validates :patient_num, uniqueness: true



    def generate_code(size=9)

      character_set = %w{ 0 1 2 3 4 5 6 7 8 9 A B C D E F G H I J K L M N O P Q R S T U V W X Y Z}
      (0...9).map{ character_set.to_a[ rand(character_set.size) ] }.join
      
  end

  def set_patient_num
      self.patient_num = generate_code(9)
  end


end
