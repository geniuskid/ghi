class Staff < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  after_create :set_staff_id
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable, :trackable, :lockable, :timeoutable

  enum user_type: [:receptionist, :doctor, :nurse, :pharmacist, :laboratorist, :accountant, :administrator]

  private

  def set_staff_id

  end

end
