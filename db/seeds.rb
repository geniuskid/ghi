# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
# User.create(email: 'admin@example.com', password: '123456')
# 100.times do
#   Patient.create(
#     patient_first_name: Faker::Name,
#     patient_last_name: Faker::Name,
#     phone_number: Faker::Number.number(7),
#     address: Faker::Address.full_address,
#     dob: Faker::Date.birthday(min_age = 18, max_age = 65)
#   )
# end

@patients = Patient.all
@patients.each do |patient|
  10.times do
    hospital = Hospital.last
    Apartment::Tenant.switch(hospital.subdomain) do
      staff = Staff.doctor.order("RANDOM()").first
      # binding.pry

    Visit.create(
      name: Faker::Name,
      phone_number: Faker::Number.number(7),
      address: Faker::Address.full_address,
      patient_id: patient.id,
      hospital_id: hospital.id ,
      doctor_name: "#{staff.first_name}",
      diagnose_1: Faker::Lorem.sentence(5) ,
      diagnose_2: Faker::Lorem.sentence(3),
      diagnose_3: Faker::Lorem.sentence(4),
      diagnose_4: Faker::Lorem.sentence(6),
      diagnose_5: Faker::Lorem.sentence(4),
      diagnose_6: Faker::Lorem.sentence(5),
      hemoglobin: Faker::Number.decimal(2),
      rbc: Faker::Number.decimal(2),
      htc: Faker::Number.decimal(2),
      mvc: Faker::Number.decimal(2),
      mch: Faker::Number.decimal(2),
      mchc: Faker::Number.decimal(2),
      rdw_cv: Faker::Number.decimal(2),
      rdw_sd: Faker::Number.decimal(2),
      wbc: Faker::Number.decimal(2),
      neuper: Faker::Number.decimal(2),
      granum: Faker::Number.decimal(2),
      plt: Faker::Number.decimal(2),
      esr: Faker::Number.decimal(2),
      symptoms_1: Faker::Lorem.sentence(6),
      symptoms_2: Faker::Lorem.sentence(7) ,
      symptoms_3: Faker::Lorem.sentence(8),
      symptoms_4: Faker::Lorem.sentence(5),
      symptoms_5: Faker::Lorem.sentence(4),
      symptoms_6: Faker::Lorem.sentence(8),
      prescription_1: Faker::Lorem.sentence(10),
      prescription_2: Faker::Lorem.sentence(7),
      prescription_3: Faker::Lorem.sentence(7),
      prescription_4: Faker::Lorem.sentence(3),
      prescription_5: Faker::Lorem.sentence(6),
      prescription_6: Faker::Lorem.sentence(8),
    )
  end
      end
end
# User.create! :email => 'admin@example.com', :password => '123456', :password_confirmation => '123456'
