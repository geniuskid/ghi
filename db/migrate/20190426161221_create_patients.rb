class CreatePatients < ActiveRecord::Migration[5.2]
  def change
    create_table :patients do |t|
      t.jsonb :personal_details

      t.timestamps
    end
  end
end
