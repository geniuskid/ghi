class AddPatientNumToPatients < ActiveRecord::Migration[5.2]
  def change
    add_column :patients, :patient_num, :string
  end
end
