class AddFieldsToStaffs < ActiveRecord::Migration[5.2]
  def change
    add_column :staffs, :first_name, :string
    add_column :staffs, :last_name, :string
    add_column :staffs, :phone_number, :integer
  end
end
