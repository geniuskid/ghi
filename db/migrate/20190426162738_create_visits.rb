class CreateVisits < ActiveRecord::Migration[5.2]
  def change
    create_table :visits do |t|
      t.references :patient, foreign_key: true
      t.jsonb :diagnose
      t.jsonb :prescription
      t.jsonb :appointment
      t.jsonb :lab_results
      t.jsonb :symptoms
      t.integer :hospital_id
      t.timestamps
    end
  end
end
