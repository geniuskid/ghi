class AddAddedByToPatients < ActiveRecord::Migration[5.2]
  def change
    add_column :patients, :added_by, :integer
  end
end
