[1mdiff --git a/Gemfile b/Gemfile[m
[1mindex 414dbed..b67faa4 100644[m
[1m--- a/Gemfile[m
[1m+++ b/Gemfile[m
[36m@@ -22,6 +22,8 @@[m [mgem 'coffee-rails', '~> 4.2'[m
 gem 'turbolinks', '~> 5'[m
 # Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder[m
 gem 'jbuilder', '~> 2.5'[m
[32m+[m[32mgem "clearance"[m
[32m+[m
 # Use Redis adapter to run Action Cable in production[m
 # gem 'redis', '~> 4.0'[m
 # Use ActiveModel has_secure_password[m
[1mdiff --git a/Gemfile.lock b/Gemfile.lock[m
[1mindex ff305ad..21c964c 100644[m
[1m--- a/Gemfile.lock[m
[1m+++ b/Gemfile.lock[m
[36m@@ -47,6 +47,7 @@[m [mGEM[m
     archive-zip (0.12.0)[m
       io-like (~> 0.3.0)[m
     arel (9.0.0)[m
[32m+[m[32m    bcrypt (3.1.12)[m
     bindex (0.7.0)[m
     bootsnap (1.4.4)[m
       msgpack (~> 1.0)[m
[36m@@ -65,6 +66,13 @@[m [mGEM[m
     chromedriver-helper (2.1.1)[m
       archive-zip (~> 0.10)[m
       nokogiri (~> 1.8)[m
[32m+[m[32m    clearance (1.17.0)[m
[32m+[m[32m      actionmailer (>= 3.1)[m
[32m+[m[32m      activemodel (>= 3.1)[m
[32m+[m[32m      activerecord (>= 3.1)[m
[32m+[m[32m      bcrypt[m
[32m+[m[32m      email_validator (~> 1.4)[m
[32m+[m[32m      railties (>= 3.1)[m
     coffee-rails (4.2.2)[m
       coffee-script (>= 2.2.0)[m
       railties (>= 4.0.0)[m
[36m@@ -74,6 +82,8 @@[m [mGEM[m
     coffee-script-source (1.12.2)[m
     concurrent-ruby (1.1.5)[m
     crass (1.0.4)[m
[32m+[m[32m    email_validator (1.6.0)[m
[32m+[m[32m      activemodel[m
     erubi (1.8.0)[m
     execjs (2.7.0)[m
     ffi (1.10.0)[m
[36m@@ -198,6 +208,7 @@[m [mDEPENDENCIES[m
   byebug[m
   capybara (>= 2.15)[m
   chromedriver-helper[m
[32m+[m[32m  clearance[m
   coffee-rails (~> 4.2)[m
   jbuilder (~> 2.5)[m
   listen (>= 3.0.5, < 3.2)[m
[1mdiff --git a/db/seeds.rb b/db/seeds.rb[m
[1mindex 1beea2a..c2bfea5 100644[m
[1m--- a/db/seeds.rb[m
[1m+++ b/db/seeds.rb[m
[36m@@ -5,3 +5,4 @@[m
 #[m
 #   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])[m
 #   Character.create(name: 'Luke', movie: movies.first)[m
[32m+[m[32mUser.create(username: 'admin', password: '123456')[m
