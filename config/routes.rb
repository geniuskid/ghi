class SubdomainPresent
  def self.matches?(request)
    if Rails.env == "production"
        request.subdomain.present?
    elsif Rails.env == "staging"
      request.subdomain.present?
    else
      request.subdomain.present?
      end
  end
end

class SubdomainBlank
  def self.matches?(request)
    if Rails.env == "production"
        request.subdomain.blank?
    elsif Rails.env == "staging"
        request.subdomain.blank?
    else
        request.subdomain.blank?
    end
  end
end

Rails.application.routes.draw do


  get 'visits/create'
  get 'visits/update'
  get 'staffs/index'
  get 'staffs/new'
  get 'staffs/show'
  get 'staffs/edit'
  post 'create_staff', to: 'staffs#create_staff'
  # devise_for :users
  # constraints(SubdomainPresent) do
  #   root 'pages#subindex', as: :subdomain_root
  # end
  constraints(SubdomainBlank) do
    root 'pages#index'
  end

    devise_for :staffs, path: 'staffs', controllers: { sessions: "staffs/sessions", registrations: 'staffs/registrations', confirmations: 'staffs/confirmations', passwords: 'staffs/passwords', unlocks: 'staffs/unlocks'}
    devise_for :users, path: 'users', controllers: { sessions: "users/sessions", registrations: 'users/registrations', confirmations: 'users/confirmations', passwords: 'users/passwords', unlocks: 'users/unlocks'}
  resources :staffs, :path_prefix => 'my'
  get 'patients/index'
  get 'patients/show'
  get 'patients/new'
  get 'patients/edit'
  root 'pages#index'

  post 'create_visit', to: 'patients#create_visit'

  resources :patients

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  namespace :admin do
    root to: 'users#index'
    resources :users
    resources :hospitals

  end

end
