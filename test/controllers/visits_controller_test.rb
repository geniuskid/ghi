require 'test_helper'

class VisitsControllerTest < ActionDispatch::IntegrationTest
  test "should get create" do
    get visits_create_url
    assert_response :success
  end

  test "should get update" do
    get visits_update_url
    assert_response :success
  end

end
